﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Calculator.Models.Operations;
using Calculator.ViewModels;
using Calc = Calculator.Models.Calculator;

namespace Calculator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var calculator = new Calc(new Dictionary<string, IOperation>
            {
                { "add", new AddOperation() },
                { "square", new SquareOperation() },
                { "equals", new EqualsOperation() }
            });

            if (Request.HttpMethod == "POST" && Request.Params.AllKeys.Contains("stack"))
            {
                calculator.Deserialize(Request["stack"]);
                var button = Request["button"];
                calculator.Enter(button);
            }

            var bundle = new CalculatorView
            {
                Display = calculator.Display,
                CalculatorState = calculator.Serialize()
            };
            return View("~/Views/Index.cshtml", bundle);
        }
    }
}
