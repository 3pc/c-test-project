﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Calculator.Models.Operations;

namespace Calculator.Models
{
    public class Calculator
    {
        private readonly Dictionary<string, IOperation> _registeredOperators;

        private Stack<double> _operands = new Stack<double>();
        private IOperation _operator;
        private string _current;

        public Calculator(Dictionary<string, IOperation> registeredOperators)
        {
            _registeredOperators = registeredOperators;
        }

        public void Enter(string button)
        {
            if (IsPartOfNumber(button))
            {
                AppendToCurrentNumber(button);
                return;
            }

            if (button.Equals("clear"))
            {
                Clear();
                return;
            }

            if (button.Equals("pi"))
            {
                _current = Math.PI.ToString(CultureInfo.InvariantCulture);
                return;
            }

            if (!_registeredOperators.ContainsKey(button))
                return; //throw new InvalidOperationException();

            AddOperator(button);
        }

        private void AppendToCurrentNumber(string input)
        {
            if (Parse(_current + input) == null)
                return;

            _current += input;
            _current = _current.TrimStart('0');
            if (_current.Length == 0)
                _current = "0";            
        }

        private void AddOperator(string input)
        {
            var nextOperator = _registeredOperators[input];

            // evaluate last typed number sequence and put it on the stack
            if (_current == null && _operands.Count < 2)
            {
                _current = _operands.Count > 0 ? _operands.Peek().ToString(CultureInfo.InvariantCulture) : "0";
            }

            if (_current != null)
            {
                _operands.Push(Parse(_current) ?? 0);
                _current = null;
            }

            // evaluate immediately
            if (nextOperator.RequiredNumberOfOperands == 1)
            {
                nextOperator.Calculate(ref _operands);
                if (nextOperator.GetType() != typeof(EqualsOperation))
                    return;
            }

            // nextOperator needs two operands
            if (_operator == null)
            {
                _operator = nextOperator;
                return;
            }

            _operator.Calculate(ref _operands);
            _operator = nextOperator.GetType() != typeof(EqualsOperation)
                ? nextOperator
                : null;            
        }

        static double? Parse(string input)
        {
            double result;
            Double.TryParse(input, NumberStyles.Any, CultureInfo.InvariantCulture, out result);
            return result;
        }

        public string Serialize()
        {
            // format: [<current string>, <operator>, <operand1>, ..., <operandx>]
            var list = new List<string>
            {
                _current,
                _operator != null ? GetOperatorHandle(_operator) : null 
            };
            list.AddRange(_operands.ToArray().Reverse().Select(o =>o.ToString(CultureInfo.InvariantCulture)));

            return System.Web.Helpers.Json.Encode(list);
        }

        public void Deserialize(string forminput)
        {
            var list = System.Web.Helpers.Json.Decode<List<string>>(forminput);
            _current = list[0];
            if (list[1] != null && _registeredOperators.ContainsKey(list[1]))
                _operator = _registeredOperators[list[1]];
            _operands.Clear();
            for(var i=2;i<list.Count;i++)
            {
                var number = Parse(list[i]);
                if (number != null)
                    _operands.Push((double)number);
            }
        }

        private static bool IsPartOfNumber(string input)
        {
            switch (input)
            {
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9":
                case ".":
                    return true;
                default:
                    return false;
            }
        }

        public string Display
        {
            get
            {
                if (_current != null)
                    return _current;

                if (_operands.Count > 0)
                    return _operands.Peek().ToString(CultureInfo.InvariantCulture);

                return "0";
            }
        }

        private string GetOperatorHandle(IOperation op)
        {
            return (
                from item in _registeredOperators 
                where item.Value.GetType() == op.GetType() 
                select item.Key
            ).FirstOrDefault();
        }

        public void Clear()
        {
            _operands.Clear();
            _operator = null;
            _current = null;
        }
    }
}