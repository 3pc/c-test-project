﻿using System;
using System.Collections.Generic;

namespace Calculator.Models.Operations
{
    public class SquareOperation:IOperation
    {
        public int RequiredNumberOfOperands { get { return 1; } }

        public double Calculate(ref Stack<double> stack)
        {
            if (stack.Count == 0)
            {
                throw new InvalidOperationException();
            }

            var op = stack.Pop();
            var result = op * op;
            stack.Push(result);
            return result;
        }
    }
}