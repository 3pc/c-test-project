﻿using System.Collections.Generic;

namespace Calculator.Models.Operations
{
    public interface IOperation
    {
        int RequiredNumberOfOperands { get; }
        double Calculate(ref Stack<double> stack);
    }
}
