﻿using System.Collections.Generic;

namespace Calculator.Models.Operations
{
    public class EqualsOperation:IOperation
    {
        public int RequiredNumberOfOperands { get { return 1; }  }
        public double Calculate(ref Stack<double> stack)
        {
            // basically this operation does nothing
            return stack.Peek();
        }
    }
}