﻿using System;
using System.Collections.Generic;

namespace Calculator.Models.Operations
{
    public class AddOperation : IOperation
    {
        public int RequiredNumberOfOperands { get { return 2; } }

        public double Calculate(ref Stack<double> stack)
        {
            if (stack.Count < RequiredNumberOfOperands)
                throw new InvalidOperationException();

            var op1 = stack.Pop();
            var op2 = stack.Pop();
            var result = op1 + op2;

            stack.Push(result);
            return result;

        }
    }
}