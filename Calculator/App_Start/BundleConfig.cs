﻿using System.Web.Optimization;

namespace Calculator
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Scripts/javascript").Include(
                "~/Scripts/jquery-2.1.1.js",
                "~/Scripts/bootstrap.js"
            ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/calculator.css"
            ));
        }
    }
}