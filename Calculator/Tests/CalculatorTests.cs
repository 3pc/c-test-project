﻿using System.Collections.Generic;
using Calculator.Models.Operations;
using NUnit.Framework;
using Calc = Calculator.Models.Calculator;

namespace Calculator.Tests
{
    [TestFixture]
    public class CalculatorTests
    {
        private Calc _calculator;

        [SetUp]
        public void SetUp()
        {
            _calculator = new Calc(new Dictionary<string, IOperation>
            {
                { "add", new AddOperation() },
                { "square", new SquareOperation() },
                { "equals", new EqualsOperation() }
            });
        }

        [Test]
        public void SimpleInputTest()
        {
            Assert.AreEqual("0", _calculator.Display);
            _calculator.Enter("3");
            Assert.AreEqual("3", _calculator.Display);
            _calculator.Enter("0");
            Assert.AreEqual("30", _calculator.Display);
            _calculator.Enter("4");
            Assert.AreEqual("304", _calculator.Display);
        }

        [Test]
        public void MultipleZerosTest()
        {
            _calculator.Enter("0");
            _calculator.Enter("0");
            Assert.AreEqual("0", _calculator.Display);
            _calculator.Enter("2");
            Assert.AreEqual("2", _calculator.Display);
        }

        [Test]
        public void SimpleAdditionTest()
        {
            _calculator.Enter("2");
            _calculator.Enter("add");
            _calculator.Enter("3");
            _calculator.Enter("equals");
            Assert.AreEqual("5", _calculator.Display);
        }

        [Test]
        public void ChainedAdditionTest()
        {
            _calculator.Enter("2");
            _calculator.Enter("add");
            _calculator.Enter("3");
            _calculator.Enter("add");
            _calculator.Enter("4");
            _calculator.Enter("equals");
            Assert.AreEqual("9", _calculator.Display);
        }

        [Test]
        public void RepeatNumberTest()
        {
            _calculator.Enter("5");
            _calculator.Enter("add");
            _calculator.Enter("equals");
            Assert.AreEqual("10", _calculator.Display);

            _calculator.Enter("add");
            _calculator.Enter("equals");
            Assert.AreEqual("20", _calculator.Display);
        }

        [Test]
        public void MultipleEqualsTest()
        {
            _calculator.Enter("5");
            _calculator.Enter("equals");
            Assert.AreEqual("5", _calculator.Display);
            _calculator.Enter("equals");
            Assert.AreEqual("5", _calculator.Display);
        }

        [Test]
        public void UnaryOperatorTest()
        {
            _calculator.Enter("5");
            _calculator.Enter("square");
            Assert.AreEqual("25", _calculator.Display);
        }

        [Test]
        public void ChainBinaryAndUnaryOperatorsTest()
        {
            _calculator.Enter("5");
            _calculator.Enter("add");
            _calculator.Enter("2");
            _calculator.Enter("square");
            Assert.AreEqual("4", _calculator.Display);

            _calculator.Enter("equals");
            Assert.AreEqual("9", _calculator.Display);
        }

        [Test]
        public void ClearTest()
        {
            _calculator.Enter("5");
            _calculator.Enter("add");
            _calculator.Enter("2");
            _calculator.Enter("clear");
            Assert.AreEqual("0", _calculator.Display);

            _calculator.Enter("3");
            _calculator.Enter("add");
            _calculator.Enter("9");
            _calculator.Enter("equals");
            Assert.AreEqual("12", _calculator.Display);
        }
    }
}