
# Test Project: A Simple Web Calculator

This boilerplate code is used to get to know new C# MVC programmers that want to work at 3pc.

### Current status

This is a simple calculator that does all necessary calculations on the server, the advantage being that it doesn't need 
JavaScript, cookies or anything enabled on the client side. The behaviour is adapted from standard OS calculators 
(calc.exe etc.), e.g. that the input sequence "3+=" produces 6 (3 + 3) or that "3 + 5 x<sup>2</sup>" is translated as 
"3 + (5<sup>2</sup>) = 28". There exist already some unit tests for base operations.

### Your programming tasks

1. Please implement the +/- button, as well as the factorial operation (x!). Please write some corresponding unit tests (NUnit).

2. If javascript is enabled, the calculator should now use Ajax and JSON format to communicate with the server, instead of reloading 
the page after every input. But if javascript is disabled, the website should still function as before (progressive enhancement).

3. Optional: If you find bugs or other errors in the existing code, we would appreciate it if you could report them, preferably 
along with a failing unit test.